//
//  ProfsGateway.swift
//  Hochschulapp
//
//  Created by Prof. Dr. Nunkesser, Robin on 06.07.17.
//  Copyright © 2017 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

protocol FetchGateway {
    
    associatedtype Model : Codable

    static func fetch(completion: @escaping
        (Result<Model,Error>) -> Void)
    
}
