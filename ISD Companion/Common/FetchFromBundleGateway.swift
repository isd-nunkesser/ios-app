//
//  FetchFromBundleGateway.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 18.03.19.
//  Copyright © 2019 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

protocol FetchFromBundleGateway : FetchGateway {
    static var resource : String {get}
}

extension FetchFromBundleGateway {
    static func fetch(completion: @escaping (Result<Model,Error>) -> Void) {
        
        if let path = Bundle.main.path(forResource: resource, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                let model = try decoder.decode(Model.self,
                                               from: data)
                completion(Result<Model,Error>.success(model))
            } catch {
                completion(Result.failure(error))
            }
        }
    }
}
