import SwiftUI

struct SubtitleRow: View {
    var text: LocalizedStringKey
    var detailText: LocalizedStringKey

    init(item: ItemViewModel) {
        self.init(text: item.text, detailText: item.detailText)
    }
    
    init(text: LocalizedStringKey, detailText: LocalizedStringKey) {
        self.text = text
        self.detailText = detailText
    }

    var body: some View {
        VStack(alignment: .leading) {
            Text(text)
                .font(.subheadline)
            Text(detailText)
                .font(.caption)
        }
    }
}

struct SubtitleRow_Previews: PreviewProvider {
    static var previews: some View {
        List(0 ..< 5) { item in
            SubtitleRow(item: ItemViewModel(text: "T", detailText: "D"))
        }
    }
}
