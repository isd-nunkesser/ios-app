//
//  Colors.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 10.04.19.
//  Copyright © 2019 Hochschule Hamm-Lippstadt. All rights reserved.
//

import UIKit

class Colors {
    
    public static let themeColors = [
        "Mint" :UIColor(rgb: 0x00C6AD),
        "Green" :UIColor(rgb: 0x02D286),
        "SkyBlue" :UIColor(rgb: 0x38A9E0),
        "ForestGreen" :UIColor(rgb: 0x3E7054),
        "NavyBlue" :UIColor(rgb: 0x425B71),
        "Teal" :UIColor(rgb: 0x468294),
        "Blue" :UIColor(rgb: 0x627AAF),
        "Brown" :UIColor(rgb: 0x735744),
        "Plum" :UIColor(rgb: 0x744670),
        "Purple" :UIColor(rgb: 0x8974CD),
        "Marnoon" :UIColor(rgb: 0x8E4238),
        "Magenta" :UIColor(rgb: 0xAF70C0),
        "Lime" :UIColor(rgb: 0xB2CF55),
        "Coffee" :UIColor(rgb: 0xB49885),
        "Orange" :UIColor(rgb: 0xEF9235),
        "Red" :UIColor(rgb: 0xF3674E),
        "WaterLemon" :UIColor(rgb: 0xF8888C),
        "Pink" :UIColor(rgb: 0xFD94CE),
    ]
    
    public static var SeperatorColor = UIColor(rgb: 0xdfdfdf)
    public static var BlankAreaColor = UIColor(rgb: 0xf5f5f5)
    public static var WhiteBackColor = UIColor(rgb: 0xffffff)
    
}
