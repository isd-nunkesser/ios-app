//  SimpleWebViewController.swift

import UIKit
import WebKit

class SimpleWebViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    var url : URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
