import Foundation
import SwiftUI

struct ItemViewModel : Identifiable {
    let id = UUID()    
    var text: LocalizedStringKey
    var detailText: LocalizedStringKey
}
