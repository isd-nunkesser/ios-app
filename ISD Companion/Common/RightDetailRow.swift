import SwiftUI

struct RightDetailRow: View {
    var text: LocalizedStringKey
    var detailText: LocalizedStringKey

    init(item: ItemViewModel) {
        self.init(text: item.text, detailText: item.detailText)
    }
    
    init(text: LocalizedStringKey, detailText: LocalizedStringKey) {
        self.text = text
        self.detailText = detailText
    }

    var body: some View {
        HStack {
            Text(text)
            Spacer()
            Text(detailText)
        }
    }
}

struct RightDetailRow_Previews: PreviewProvider {
    static var previews: some View {
        List(0 ..< 5) { item in
            RightDetailRow(text: "T", detailText: "D")
        }
    }
}
