//
//  MealGateway.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

protocol MealGateway {
    static func fetchMeals(completion: @escaping
        (Result<[MealEntity],Error>)
        -> Void)
}
