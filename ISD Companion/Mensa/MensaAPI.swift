//
//  MensaAPI.swift
//  Hochschulapp
//
//  Created by Prof. Dr. Nunkesser, Robin on 16.05.17.
//  Copyright © 2017 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

struct MensaAPI {
    
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()

    enum RequestKeys: String {
        case id = "id"
        case restaurant = "restaurant"
        case date = "date"
    }
    
    enum ResponseKeys: String {
        case date = "date"
        case name = "name_"
        case priceGuests = "priceGuests"
        case priceStudents = "priceStudents"
        case priceWorkers = "priceWorkers"
        case category = "category"
        case image = "image"
        case allergens = "allergens"
    }
        
    enum Category : String {
        case dish = "dish"
        case sidedish = "sidedish"
        case dessert = "dessert"
        case soups = "soups"
        case dishDefault = "dish-default"
        case dessertCounter = "dessert-counter"
        case dishGrill = "dish-grill"
    }
    
    static func keyForHamm() -> String {
        return "mensa-hamm"
    }
        
    static func mealsFromJSONData(json: [[String:AnyObject]])
                                  -> [MealEntity] {
        var items: [MealEntity] = []
        var locale = Locale.current.languageCode!
        if (!Set(["de","en"]).contains(locale)) {
            locale = "en"
        }
        for mealJSON in json {
            if let dateString = mealJSON[ResponseKeys.date.rawValue]
                                as? String,
            let date = dateFormatter.date(from: dateString),
            let name = mealJSON["\(ResponseKeys.name.rawValue)\(locale)"]
                       as? String,
            let priceG = mealJSON[ResponseKeys.priceGuests.rawValue]
                         as? Double,
            let priceS = mealJSON[ResponseKeys.priceStudents.rawValue]
                         as? Double,
            let priceW = mealJSON[ResponseKeys.priceWorkers.rawValue]
                         as? Double,
            let categoryString = mealJSON[ResponseKeys.category.rawValue]
                                 as? String,
            let image = mealJSON[ResponseKeys.image.rawValue] as? String,
            let allergens = mealJSON[ResponseKeys.allergens.rawValue]
                            as? [String]
            {
                let item = MealEntity(date: date, name: name,
                                price: (priceG, priceS, priceW),
                                category:
                           MealEntityCategory(rawValue: categoryString)!,
                                image: URL(string: image),
                                allergens: Set(allergens))
                
                items.append(item)
            }
        }
        return items
    }
}
