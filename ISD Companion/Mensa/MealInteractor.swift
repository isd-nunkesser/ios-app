//
//  MealInteractor.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

class MealInteractor : UseCase {
    typealias DisplayerType = MensaViewController
    typealias PresenterType = MealPresenter
    typealias RequestType = Void?
    
    var presenter: MealPresenter
    var gateway: MealGateway
    
    init(presenter : MealPresenter, gateway: MealGateway) {
        self.presenter = presenter
        self.gateway = gateway
    }
    
    required convenience init(presenter : MealPresenter) {
        self.init(presenter: presenter, gateway: ConcreteMealGateway())
    }
    
    convenience init() {
        self.init(presenter: MealPresenter(), gateway: ConcreteMealGateway())
    }

    func execute(request: Void?, displayer: MensaViewController, resultCode: Int) {
        ConcreteMealGateway.fetchMeals(completion:
            {
                switch $0 {
                case let .success(meals):
                    displayer.display(success: meals, resultCode: resultCode)
                case let .failure(error):
                    displayer.display(failure: error)
                }
        })
    }
    
}
