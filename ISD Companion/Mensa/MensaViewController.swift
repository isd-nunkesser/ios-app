//
//  MensaViewController.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import UIKit
import BasicCleanArch

//protocol MealOutputBoundary : EntityListOutputBoundary where Entity == MealEntity {}

class MensaViewController : UITableViewController, Displayer {
    typealias ViewModelType = [MealEntity]
    
    // MARK: - Properties
    
    var interactor = MealInteractor()
    
    var meals: [MealViewModel] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var filter: Set<String> = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var filteredMeals: [MealViewModel] {
        return meals.filter {
            $0.allergens.intersection(filter).isEmpty
        }
    }
    
    var dishes: [MealViewModel] {
        return filteredMeals.filter {
            $0.category == MealViewModelCategory.dish
        }
    }
    
    var sidedishes: [MealViewModel] {
        return filteredMeals.filter {
            $0.category == MealViewModelCategory.sidedish
        }
    }
    
    var soups: [MealViewModel] {
        return filteredMeals.filter {
            $0.category == MealViewModelCategory.soups
        }
    }
    
    var desserts: [MealViewModel] {
        return filteredMeals.filter {
            $0.category == MealViewModelCategory.dessert
        }
    }
    
    // MARK: - Methods
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        filter = AllergeneSettings().allergenes
        interactor.execute(request: nil, displayer: self)
    }        
    
    // MARK: IBActions
    
    @IBAction func showInformation(_ sender: UIBarButtonItem) {
        let alertView = UIAlertController(
            title: NSLocalizedString("Info", comment: ""),
            message: NSLocalizedString("Mensa.info", comment: ""),
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(
            title: NSLocalizedString("OK", comment: ""),
            style: .cancel,
            handler: nil)
        alertView.addAction(okAction)
        
        let showAction = UIAlertAction(title: NSLocalizedString("Show", comment: ""), style: .default, handler:
        {_ in
            UIApplication.shared.open(URL(string: NSLocalizedString("Mensa.infopage", comment: ""))!, options: [:],
                                      completionHandler: nil)
            
        })
        alertView.addAction(showAction)
        self.present(alertView, animated: true, completion: nil)
    }
        
    func display(success: [MealEntity], resultCode: Int) {
        self.meals = success.map(MealPresenter().present)
    }
    
    func display(failure: Error) {
        self.present(error: failure, handler: nil)
    }
    
    func sectionInfo(section: Int) -> (String, [MealViewModel]) {
        switch section {
        case 0: return (MealViewModelCategory.dish.localizedName!, dishes)
        case 1: return (MealViewModelCategory.soups.localizedName!, soups)
        case 2: return (MealViewModelCategory.sidedish.localizedName!, sidedishes)
        case 3: return (MealViewModelCategory.dessert.localizedName!, desserts)
        default: return ("", [])
        }
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return MealViewModelCategory.count
    }
    
    override func tableView(_ tableView: UITableView,
                            titleForHeaderInSection section: Int) -> String? {
        return sectionInfo(section: section).0
    }
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return sectionInfo(section: section).1.count
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath)
        -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(
                withIdentifier: MensaTableViewCell.reuseIdentifier,
                for: indexPath) as? MensaTableViewCell
            let mealItem = sectionInfo(section: indexPath.section).1[indexPath.row]
            cell?.foodTitleLabel.text = mealItem.name
            cell?.priceLabel.text = mealItem.price
            cell?.mealImage.image = nil
            if let imageURL = mealItem.image {
                cell?.mealImage.imageFromUrl(url: imageURL)
            }
            return cell!
    }

    @objc func willEnterForeground(notification: Notification) {
        let allergenes = AllergeneSettings().allergenes
        if (allergenes != filter) {
            filter = allergenes
        }
    }
    
}
