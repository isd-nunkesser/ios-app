//
//  MealViewModel.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 05.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

struct MealViewModel
{
    var name: String
    var price: String
    var category: MealViewModelCategory
    var image: URL?
    var allergens: Set<String>
    
    init(name: String,
         price: String,
         category: MealViewModelCategory,
         image: URL?,
         allergens: Set<String>) {
        self.name = name
        self.price = price
        self.category = category
        self.image = image
        self.allergens = allergens
    }
}
