//
//  MealEntity.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

enum MealFailure: String {
    case noMeals
}

extension MealFailure: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noMeals:
            return NSLocalizedString("NoMeals.error", comment: "")
        }
    }
}

struct MealEntity
{
    var date : Date
    var name : String
    var price: (Double,Double,Double)
    var category: MealEntityCategory
    var image: URL?
    var allergens: Set<String>
    
    init(date: Date,
    name: String,
    price: (Double,Double,Double),
    category: MealEntityCategory,
    image: URL?,
    allergens: Set<String>) {
        self.date = date
        self.name = name
        self.price = price
        self.category = category
        self.image = image
        self.allergens = allergens
}
}
