//
//  MealEntityCategory.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

enum MealEntityCategory : String {
    case dish = "dish"
    case sidedish = "sidedish"
    case dessert = "dessert"
    case soups = "soups"
    case dishDefault = "dish-default"
    case dessertCounter = "dessert-counter"
    case dishGrill = "dish-grill"
}
