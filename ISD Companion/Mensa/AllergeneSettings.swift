//
//  AllergeneSettings.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 19.03.19.
//  Copyright © 2019 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

class AllergeneSettings {
    let userDefaults = UserDefaults.standard
    init() {
        for i in 1...14 {
            if !userDefaults.bool(forKey: "A\(i)") {
                allergenes.insert("A\(i)")
            }
        }
        for i in 1...15 {
            if !userDefaults.bool(forKey: "Z\(i)") {
                allergenes.insert("\(i)")
            }
        }
    }
    
    var allergenes: Set<String> = []
}
