//
//  MealPresenter.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

class MealPresenter : Presenter {
    typealias Model = MealEntity
    typealias ViewModel = MealViewModel
    
    let euroFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = "EUR"
        return formatter
    }()
    
    func present(model: MealEntity) -> MealViewModel {
        
        let priceGuests = euroFormatter.string(
            from: NSNumber(floatLiteral: model.price.0))
        let priceStudents = euroFormatter.string(
            from: NSNumber(floatLiteral: model.price.1))
        let priceWorkers = euroFormatter.string(
            from: NSNumber(floatLiteral: model.price.2))
        
        let price =
        "\(priceStudents!) / \(priceWorkers!) / \(priceGuests!)"
        
        var category : MealViewModelCategory
        switch model.category {
        case .dish, .dishDefault, .dishGrill : category = .dish
        case .dessert, .dessertCounter: category = .dessert
        case .sidedish: category = .sidedish
        case .soups: category = .soups
        }
        return MealViewModel(name: model.name, price: price,
                               category: category, image: model.image,
                               allergens: model.allergens)
    }
    
}
