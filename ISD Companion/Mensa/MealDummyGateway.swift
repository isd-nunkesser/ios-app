//
//  MealDummyGateway.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

class ConcreteMealGateway : MealGateway {
    static func fetchMeals(completion: @escaping (Result<[MealEntity], Error>) -> Void) {
        completion(Result<[MealEntity], Error>.success([
            MealEntity(date: Date(),
                       name: "Test",
                       price: (0.1,0.3,0.4),
                       category: MealEntityCategory.dessert,
                       image: URL(string: "https://www.nrn.com/sites/nrn.com/files/styles/article_featured_standard/public/burger_0.gif?itok=1dIeFxgf")!,
                       allergens: [])]))
    }
}
