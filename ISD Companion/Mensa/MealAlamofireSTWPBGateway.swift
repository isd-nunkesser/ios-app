//
//  MensaAlamofireSTWPBGateway.swift
//  Hochschulapp
//
//  Created by Prof. Dr. Nunkesser, Robin on 13.06.17.
//  Copyright © 2017 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import Alamofire

struct ConcreteMealGateway : MealAlamofireGateway {
    static let url: String = "http://www.studentenwerk-pb.de/fileadmin/shareddata/access2.php"
    static let secret = NSDictionary(contentsOfFile:
                        Bundle.main.path(forResource: "Secrets",
                                         ofType: "plist")!)!["MensaSecret"]
                        as! String
    static let method : HTTPMethod = .get
    static let encoding: ParameterEncoding = URLEncoding.default
            
    static func getParameters(date: Date?)
                -> Parameters?
    {
        var parameters : Parameters =
            [MensaAPI.RequestKeys.id.rawValue:secret,
             MensaAPI.RequestKeys.restaurant.rawValue:
             MensaAPI.keyForHamm()]
        if let givenDate = date {
            parameters[MensaAPI.RequestKeys.date.rawValue] =
            MensaAPI.dateFormatter.string(from: givenDate)
        }
        return parameters
    }
    
    static func getJSONArray(json: Any?) -> [[String : AnyObject]]? {
        if let resultsArray = json as? [[String:AnyObject]] {
            return resultsArray
        } else {
            return nil
        }
    }
}
