//
//  MensaAlamofireGateway.swift
//  Hochschulapp
//
//  Created by Prof. Dr. Nunkesser, Robin on 16.05.17.
//  Copyright © 2017 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import Alamofire
import BasicCleanArch

protocol MealAlamofireGateway : MealGateway {
    static var url : String {get}
    static var method : HTTPMethod {get}
    static var encoding : ParameterEncoding {get}        
    static func getParameters(date: Date?)
                -> Parameters?
    static func getJSONArray(json: Any?) -> [[String:AnyObject]]?
}

extension MealAlamofireGateway {
    static func fetchMeals(completion: @escaping
                           (Result<[MealEntity],Error>)
                           -> Void)
    {
        let parameters = getParameters(date: Date())
        AF.request(url,
                          method: method,
                          parameters: parameters,
                          encoding: encoding)
                 .validate()
                 .responseJSON
            {
                alamoResponse in
                var response : Result<[MealEntity],Error>
                response = Result.failure(MealFailure.noMeals)
                switch alamoResponse.result {
                case .success:
                    var items: [MealEntity] = []
                    let results = alamoResponse.value
                    if let resultsArray = getJSONArray(json: results) {
                        items = MensaAPI
                                .mealsFromJSONData(json: resultsArray)
                    }
                    
                    if (items.count > 0) {
                        response = Result<[MealEntity],Error>
                                   .success(items)
                    }
                case .failure(let error):
                    response = Result.failure(error)
                }
                completion(response)
            }
    }    
}
