//
//  MensaTableViewCell.swift
//  Hochschulapp
//
//  Created by Prof. Dr. Nunkesser, Robin on 24.05.17.
//  Copyright © 2017 Hochschule Hamm-Lippstadt. All rights reserved.
//

import UIKit

class MensaTableViewCell: UITableViewCell {
    
    // MARK: Properties    
    static let reuseIdentifier = "MensaCell"
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var foodTitleLabel: UILabel!
    @IBOutlet weak var mealImage: UIImageView!
}
