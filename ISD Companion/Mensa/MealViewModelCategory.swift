//
//  MealViewModelCategory.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 05.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

enum MealViewModelCategory : String {
    case dish = "header.maindishes"
    case sidedish = "header.sidedish"
    case soups = "header.soups"
    case dessert = "header.dessert"
    
    public var localizedName: String? {
        return NSLocalizedString(self.rawValue, comment: "")
    }
    
    static let count = 4
}
