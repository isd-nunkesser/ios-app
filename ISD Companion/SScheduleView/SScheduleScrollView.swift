//
//  SScheduleScrollView.swift
//  SScheduleView
//
//  Created by 黄帅 on 2017/3/21.
//  Copyright © 2017年 huangshuai. All rights reserved.
//

import UIKit
//import MJRefresh

public protocol SScheduleScrollViewDelegate:class {
    func pullSScheduleScrollViewStartRefreshing(_ ahScheduleScrollView:SScheduleScrollView)
}

open class SScheduleScrollView: UIScrollView {
    weak var pullDelegate:SScheduleScrollViewDelegate?
    
    var isLoadingMore:Bool = false
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
}
