//
//  SScheduleCourseView.swift
//  SScheduleView
//
//  Created by 黄帅 on 2017/3/27.
//  Copyright © 2017年 huangshuai. All rights reserved.
//

import UIKit
import SnapKit
public protocol SScheduleCourseViewProtocol:class {
    func tapCourse(courseModel:ScheduleEntryViewModel)
}

open class SScheduleCourseView: UIView {
    
    public var model:ScheduleEntryViewModel!
    
    private var courseInfoButton:UIButton!
    
    public var courseInfoAlpha:CGFloat = 1.0 {
        didSet {
            courseInfoButton.backgroundColor = courseInfoButton.backgroundColor?.withAlphaComponent(courseInfoAlpha)
        }
    }
    
    public var courseInfoIsCorner:Bool = false {
        didSet {
            courseInfoButton.layer.cornerRadius = courseInfoIsCorner ? 10 : 0
        }
    }
    
    weak var delegate:SScheduleCourseViewProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open func setupUI(with model:ScheduleEntryViewModel) {
        self.model = model
        var title = "\(self.model.courseName)\n\(self.model.room)"
        if UIDevice.current.orientation.isLandscape {
            title = "\(self.model.courseName)"
        }
        
        self.backgroundColor = UIColor.clear
        let courseBackView = UIView()
        courseBackView.backgroundColor = UIColor.clear
        courseBackView.isUserInteractionEnabled = true
        addSubview(courseBackView)
        courseBackView.snp.makeConstraints{
            $0.bottom.top.right.left.equalTo(self)
        }
        
        courseInfoButton = UIButton()
        courseInfoButton.setTitle(title, for: UIControl.State.normal)
        courseInfoButton.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        
        courseInfoButton.contentHorizontalAlignment = .center
        courseInfoButton.contentVerticalAlignment = .center
        courseInfoButton.contentEdgeInsets = UIEdgeInsets(top: 1, left: 2, bottom: 1, right: 2)
        courseInfoButton.titleLabel?.numberOfLines = 0
        courseInfoButton.backgroundColor = self.model.backColor 
                
        courseInfoButton.addTarget(self, action: #selector(self.courseInfoButtonTouch(sender:)), for: UIControl.Event.touchUpInside)
        
        addSubview(courseInfoButton)
        courseInfoButton.snp.makeConstraints{
            $0.edges.equalTo(courseBackView).inset(UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1))
        }
    }
    
    @objc fileprivate func courseInfoButtonTouch(sender:UIButton) {
        delegate?.tapCourse(courseModel: self.model)
    }
}
