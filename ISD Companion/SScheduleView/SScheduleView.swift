//
//  SScheduleView.swift
//  SScheduleView
//
//  Created by 黄帅 on 2017/3/21.
//  Copyright © 2017年 huangshuai. All rights reserved.
//

import UIKit
import SnapKit

public protocol SScheduleViewDelegate:class {
    func tapCourse(courseModel:ScheduleEntryViewModel)
    func swipeGestureRight()
    func swipeGestureLeft()
}

open class SScheduleView: UIView {
    @IBInspectable var sideColWidthRatio:CGFloat = 2 {
        didSet {
            resetLayout()
        }
    }

    @IBInspectable var headViewBackgroundColor: UIColor = Colors.BlankAreaColor {
        didSet {
            resetLayout()
        }
    }

    @IBInspectable var sideViewBackgroundColor: UIColor = Colors.BlankAreaColor {
        didSet {
            resetLayout()
        }
    }
    
    var courseDataList = Array<ScheduleEntryViewModel>()
    
    open weak var delegate:SScheduleViewDelegate?
    
    fileprivate var daysRange : Range<Int> {
        let wholeWeek = 0..<5
        if UIDevice.current.orientation.isLandscape {
            return wholeWeek
        } else {
            if !wholeWeek.contains(weekDay) {
                weekDay = 0
            }
            return weekDay..<weekDay+1
        }
    }
    
    var weekDay = Calendar.current.component(.weekday, from: Date.init())-2
    fileprivate var showJiesNum:CGFloat = 12
    fileprivate var showWeekNum:Int = 1
    fileprivate var termStartDate = Date()
    fileprivate var isShowWeekNum: Bool = true
    
    open var firstColumnWidth:CGFloat!
    open var notFirstEveryColumnsWidth:CGFloat!
    open var firstRowHeight:CGFloat!
    open var notFirstEveryRowHeight:CGFloat!
    
    fileprivate let titleNames = [NSLocalizedString("wdshort.mon", comment: ""),NSLocalizedString("wdshort.tue", comment: ""),NSLocalizedString("wdshort.wed", comment: ""),NSLocalizedString("wdshort.thu", comment: ""),NSLocalizedString("wdshort.fri", comment: "")]
    public static let timeSlotNames = ["08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00"]
    fileprivate var datesOfMonth:[String]!
    
    public var headView:UIView!
    public var sideView:UIView!
    public var contentView:UIView!
    public var courseViewList = Array<SScheduleCourseView>()
    
    public var backImageView:UIImageView!
    
    open func setShowWeek(with num:Int) {
        self.showWeekNum = num
        updateHeadView()
    }
    
    open func setWeekIsShow(with isShow:Bool) {
        self.isShowWeekNum = isShow
        updateHeadView()
    }
    
    open func setTermStartDate(with dateString:String) {
        let dateFmt = DateFormatter()
        dateFmt.dateFormat = "yyyy-MM-dd"
        self.termStartDate = dateFmt.date(from: dateString)!
        let from = self.termStartDate.timeIntervalSince1970
        let now = (Date().timeIntervalSince1970)
        var week = Int((now - from)/(7 * 24 * 3600)) + 1
        if week < 1 {
            week = 0
        }
        self.showWeekNum = week
        updateHeadView()
    }
    
    open func clearOldCourseView() {
        _ = contentView.subviews.map {
            $0.removeFromSuperview()
        }
    }
    
    open func updateCourseView(courseDataList:Array<ScheduleEntryViewModel>) {
        self.courseDataList = courseDataList
        clearOldCourseView()
        updateCourseView()
    }
    
    fileprivate var courseViewIsCorner:Bool = false
    
    open func setCourseViewIsCorner(_ isCorner:Bool) {
        for course in courseViewList {
            self.courseViewIsCorner = isCorner
            course.courseInfoIsCorner = isCorner
        }
    }
    
    open func getDataModel(with location: CGPoint) -> ScheduleEntryViewModel? {
        for view in contentView.subviews {
            if view.frame.contains(location) && view is SScheduleCourseView{
                return (view as! SScheduleCourseView).model
            }
        }
        return nil
    }
    
    fileprivate var backImg:UIImage?

    open func setBackground(with image:UIImage?) {
        backImg = image
        setBackground()
    }
    
    fileprivate func setBackground() {
        if let backImage = backImg {
            self.headView.backgroundColor = headViewBackgroundColor.withAlphaComponent(0.2)
            self.sideView.backgroundColor = sideViewBackgroundColor.withAlphaComponent(0.2)
            self.backImageView.image = backImage
        } else {
            self.headView.backgroundColor = headViewBackgroundColor
            self.sideView.backgroundColor = sideViewBackgroundColor
            self.backImageView.image = nil
        }
    }
    
    fileprivate  var courseViewAlpha:CGFloat = 1.0
    
    /// 设置课程格子的透明度（优化背景效果）
    ///
    /// - Parameter value: alpha
    open func setCourseViewsAlpha(with value:CGFloat) {
        guard value > 0.0 && value <= 1.0 else {
            return
        }
        
        courseViewAlpha = value
        
        for course in courseViewList {
            course.courseInfoAlpha = value
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initUI()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    override open func prepareForInterfaceBuilder() {
        initUI()
    }
    
    fileprivate func resetLayout() {
        _ = self.subviews.map { view in
            view.removeFromSuperview()
        }
        
        initUI()
        updateCourseView()
    }
    
    fileprivate func initUI() {
        initUISize()
        
        backImageView = UIImageView()
        addSubview(backImageView)
        backImageView.snp.makeConstraints{
            $0.left.top.right.bottom.equalTo(self)
        }
        
        drawFirstRow()
        drawOtherRows()
        addContentViewGesture()
        NotificationCenter.default.addObserver(self, selector: #selector(orientationChanged), name: UIDevice.orientationDidChangeNotification, object: nil)
        setBackground()
    }
    
    @objc func orientationChanged(notification: Notification) {
        resetLayout()
    }
    
    open func initUISize() {
        firstColumnWidth = self.bounds.width / (sideColWidthRatio * CGFloat(daysRange.count) + 1)
        notFirstEveryColumnsWidth = firstColumnWidth * sideColWidthRatio
        
        firstRowHeight = 30
        notFirstEveryRowHeight = (self.bounds.height - firstRowHeight) / showJiesNum
    }
    
    open func drawFirstRow() {
        headView = UIView()
        headView.backgroundColor = headViewBackgroundColor
        
        addSubview(headView)
        headView.snp.makeConstraints{
            $0.left.top.right.equalTo(self)
            $0.height.equalTo(firstRowHeight)
        }
        drawFirstRowOtherColCell()
    }
    
    open func drawOtherRows() {
        let scrollView = SScheduleScrollView()
        addSubview(scrollView)
        scrollView.snp.makeConstraints{
            $0.top.equalTo(self).offset(firstRowHeight)
            $0.right.left.bottom.equalTo(self)
            $0.width.equalTo(self.snp.width)
        }
        
        let scrollCotentView = UIView()
        scrollView.addSubview(scrollCotentView)
        scrollCotentView.snp.makeConstraints{
            $0.edges.equalTo(scrollView)
            $0.center.equalTo(scrollView)
        }
        
        drawOtherRowFirstCol(scrollCotentView: scrollCotentView)
        drawOtherRowOtherCol(scrollCotentView: scrollCotentView)
    }
    
    open func drawFirstRowOtherColCell() {
        
        var column = -1
        for i in daysRange {
            column+=1
            let view = UIView()
            headView.addSubview(view)
            view.snp.makeConstraints{
                $0.top.height.equalTo(headView)
                $0.left.equalTo(headView).offset(notFirstEveryColumnsWidth * CGFloat(column) + firstColumnWidth)
                $0.width.equalTo(notFirstEveryColumnsWidth)
            }
            
            let dayLabel = UILabel()
            dayLabel.text = titleNames[i]
            dayLabel.font = UIFont.boldSystemFont(ofSize: 10)
            view.addSubview(dayLabel)
            dayLabel.snp.makeConstraints{
                $0.centerX.top.equalTo(view)
                $0.height.equalTo(firstRowHeight)
            }
            
            let seperateView = UIView()
            seperateView.backgroundColor = Colors.SeperatorColor
            view.addSubview(seperateView)
            seperateView.snp.makeConstraints {
                $0.width.equalTo(0.5)
                $0.left.top.bottom.equalTo(view)
            }
        }
    }
    
    open func drawOtherRowFirstCol(scrollCotentView:UIView) {
        sideView = UIView()
        sideView.backgroundColor = sideViewBackgroundColor.withAlphaComponent(0.2)
        scrollCotentView.addSubview(sideView)
        sideView.snp.makeConstraints{
            $0.left.top.bottom.equalTo(scrollCotentView)
            $0.width.equalTo(firstColumnWidth)
            $0.height.equalTo(notFirstEveryRowHeight * showJiesNum)
        }
        
        for i in 0 ..< Int(showJiesNum) {
            let jieciBackView = UIView()
            jieciBackView.backgroundColor = UIColor.clear
            sideView.addSubview(jieciBackView)
            jieciBackView.snp.makeConstraints{
                $0.top.equalTo(sideView.snp.top).offset(notFirstEveryRowHeight * CGFloat(i))
                $0.right.left.equalTo(sideView)
                $0.height.equalTo(notFirstEveryRowHeight)
            }
            
            let jieciLabel = UILabel()
            jieciLabel.backgroundColor = UIColor.clear
            jieciLabel.text = SScheduleView.timeSlotNames[i]
            jieciLabel.textColor = UIColor.black
            jieciLabel.font = UIFont.systemFont(ofSize: 10)
            jieciLabel.textAlignment = NSTextAlignment.center
            jieciBackView.addSubview(jieciLabel)
            jieciLabel.snp.makeConstraints{
                $0.top.bottom.right.left.equalTo(jieciBackView)
            }
            
            let seperateView = UIView()
            seperateView.backgroundColor = Colors.SeperatorColor
            sideView.addSubview(seperateView)
            seperateView.snp.makeConstraints{
                $0.height.equalTo(0.5)
                $0.top.right.left.equalTo(jieciBackView)
            }
        }
    }
    
    open func drawOtherRowOtherCol(scrollCotentView:UIView) {
        contentView = UIView()
        scrollCotentView.addSubview(contentView)
        contentView.snp.makeConstraints{
            $0.right.equalTo(scrollCotentView)
            $0.top.bottom.equalTo(sideView)
            $0.left.equalTo(sideView.snp.right)
        }
    }
    
    open func addContentViewGesture() {
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action:#selector(self.viewSwpie(_:)))
        swipeRightGesture.direction = .right
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action:#selector(self.viewSwpie(_:)))
        swipeLeftGesture.direction = .left
        contentView.addGestureRecognizer(swipeRightGesture)
        contentView.addGestureRecognizer(swipeLeftGesture)
    }
    
    open func updateCourseView() {
        for data in courseDataList {
            if daysRange.contains(data.dayInWeek-1) {
            let courseBackView = SScheduleCourseView()
            courseBackView.delegate = self
            courseBackView.setupUI(with: data)
            contentView.addSubview(courseBackView)
            courseViewList.append(courseBackView)
            courseBackView.snp.makeConstraints{
                $0.width.equalTo(notFirstEveryColumnsWidth)
                $0.height.equalTo(notFirstEveryRowHeight * CGFloat(data.courseSpan))
                $0.left.equalTo(contentView).offset(notFirstEveryColumnsWidth * CGFloat(data.dayInWeek - daysRange.startIndex - 1))
                $0.top.equalTo(contentView).offset(notFirstEveryRowHeight * CGFloat(data.hourSlot - 1))
            }
            courseBackView.courseInfoAlpha = self.courseViewAlpha
            courseBackView.courseInfoIsCorner = self.courseViewIsCorner
            }
        }
    }
    
    @objc fileprivate func viewSwpie(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == UISwipeGestureRecognizer.Direction.right {
            if UIDevice.current.orientation.isPortrait {
                weekDay = weekDay==0 ? 4 : weekDay - 1
                resetLayout()
            }
                delegate?.swipeGestureRight()
        } else if sender.direction == UISwipeGestureRecognizer.Direction.left {
            if UIDevice.current.orientation.isPortrait {
                weekDay = (weekDay + 1) % 5
                resetLayout()
            }
            delegate?.swipeGestureLeft()
        }
    }
    
    fileprivate func updateHeadView() {
        headView.removeFromSuperview()
        drawFirstRow()
    }
}

extension SScheduleView:SScheduleCourseViewProtocol {
    public func tapCourse(courseModel:ScheduleEntryViewModel) {
        delegate?.tapCourse(courseModel: courseModel)
    }
}

extension SScheduleView {
    
    open func randomInRange(_ range: Range<Int>) -> Int {
        let count = UInt32(range.upperBound - range.lowerBound)
        return  Int(arc4random_uniform(count)) + range.lowerBound
    }
    
    public func getMonth(startTermDate:Date , week:Int) -> String {
        var startDate = Int(startTermDate.timeIntervalSince1970)
        startDate = startDate + ((week - 1) * 7 * 24 * 3600)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "M"
        
        let timeInterval = TimeInterval(startDate)
        let date = Date(timeIntervalSince1970: timeInterval)
        let string = formatter.string(from: date)
        
        return string
    }
    
    public func getDaysListForWeek(startTermDate:Date , week:Int) -> [String] {
        var startDate = Int(startTermDate.timeIntervalSince1970)
        startDate = startDate + ((week - 1) * 7 * 24 * 3600)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d"
        
        var weekString:[String] = []
        
        for i in 0..<7 {
            let day = startDate + (i * 24 * 3600)
            let timeInterval = TimeInterval(day)
            let date = Date(timeIntervalSince1970: timeInterval)
            let string = formatter.string(from: date)
            weekString.append(string)
        }
        
        return weekString
    }
}
