import UIKit

public class ScheduleEntryViewModel : CustomStringConvertible {
    public var description: String
    
    var dayInWeek = 0
    var hourSlot = 0
    var courseSpan = 0
    var courseName = ""
    var room = ""
    var timeComment = ""
    var lecturer = ""
    var backColor = Colors.themeColors["Magenta"] // 4, 6, 8, 9
    init(dayInWeek:Int,hourSlot:Int,courseSpan:Int,timeComment:String,courseName:String,lecturer:String,room:String) {
        self.dayInWeek = dayInWeek
        self.hourSlot = hourSlot
        self.courseSpan = courseSpan
        self.timeComment = timeComment
        self.courseName=courseName
        self.lecturer = lecturer
        self.room = room
        self.description = "\(timeComment)\n\(courseName)\n\(lecturer)\n\(room)"
    }
    
    
}
