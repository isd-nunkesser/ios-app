//
//  ScheduleEntryEntity.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 18.03.19.
//  Copyright © 2019 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

class ScheduleEntryEntity : Codable {
    public var dayOfWeek: Int
    public var startTime: String
    public var endTime: String
    public var timeComment: String
    public var name: String
    public var lecturer: String
    public var room: String
}
