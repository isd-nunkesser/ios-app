//
//  ScheduleInteractor.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 18.03.19.
//  Copyright © 2019 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

class ScheduleInteractor : UseCase {
    typealias PresenterType = ScheduleEntryPresenter
    typealias DisplayerType = ScheduleViewController
    typealias RequestType = ScheduleRequest
    
    var presenter: ScheduleEntryPresenter
    var gateway: ConcreteScheduleGateway
    
    init(presenter : ScheduleEntryPresenter, gateway: ConcreteScheduleGateway) {
        self.presenter = presenter
        self.gateway = gateway
    }
    
    required convenience init(presenter : ScheduleEntryPresenter) {
        self.init(presenter: presenter, gateway: ConcreteScheduleGateway())
    }
    
    convenience init() {
        self.init(presenter: ScheduleEntryPresenter(), gateway: ConcreteScheduleGateway())
    }
    
    func execute(request: ScheduleRequest, displayer: ScheduleViewController, resultCode: Int) {
        ConcreteScheduleGateway.fetch(completion:
            {
                switch $0 {
                case let .success(response):
                    let viewModel = response[request.index].entries.map(self.presenter.present)
                    displayer.display(success: viewModel, resultCode: resultCode)
                case let .failure(error):
                    displayer.display(failure: error)
                }
        })
    }
    
}
