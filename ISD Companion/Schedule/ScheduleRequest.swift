//
//  ScheduleRequest.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 19.03.19.
//  Copyright © 2019 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

class ScheduleRequest {
    var index : Int
    
    init(index:Int) {
        self.index = index
    }
}
