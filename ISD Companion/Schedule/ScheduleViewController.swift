//
//  ScheduleViewController.swift
//  Hochschulapp
//
//  Created by Prof. Dr. Nunkesser, Robin on 13.06.17.
//  Copyright © 2017 Hochschule Hamm-Lippstadt. All rights reserved.
//

import UIKit
import BasicCleanArch
import EventKitUI

class ScheduleViewController : UIViewController, Displayer, SScheduleViewDelegate, EKEventEditViewDelegate {
    
    @IBOutlet weak var scheduleView: SScheduleView!
    
    typealias ViewModelType = [ScheduleEntryViewModel]
    
    // MARK: - Properties
    var interactor = ScheduleInteractor()
    var thisWeekCourse : [ScheduleEntryViewModel] = []
    var chosenSchedule: Int = 0
    var schedules = ["ISD 2. Sem. - Gr. A","ISD 2. Sem. - Gr. B", "ISD 4. Sem. - SSP: CySe","ISD 4. Sem. - SSP: EmSy","ISD 4. Sem. - SSP: MoCo", "ISD 6. Sem. - SSP: CySe","ISD 6. Sem. - SSP: EmSy","ISD 6. Sem. - SSP: MoCo"]
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        return formatter
    }()
    let startDate = dateFormatter.date(from: "2020-04-20 08:00")!
    let startDateOddWeek = dateFormatter.date(from: "2020-04-20 08:00")!
    let startDateEvenWeek = dateFormatter.date(from: "2020-04-27 08:00")!
    let endDate = dateFormatter.date(from: "2020-06-27 08:00")!
    let calendar = NSCalendar.current
    
    // MARK: - Methods
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        chosenSchedule = UserDefaults.standard.integer(forKey: "schedulePreference")
        self.title = schedules[chosenSchedule]
        interactor.execute(request: ScheduleRequest(index: chosenSchedule), displayer: self)
        scheduleView.delegate = self
        scheduleView.setCourseViewsAlpha(with: 1)
    }
    
    // MARK: Displayer
        
    func display(success: [ScheduleEntryViewModel], resultCode: Int) {
        self.thisWeekCourse = success
        scheduleView.updateCourseView(courseDataList: thisWeekCourse)
        scheduleView.setCourseViewsAlpha(with:0.5)
    }
    
    func display(failure: Error) {
        self.present(error: failure, handler: nil)
    }
    
    // MARK: SScheduleViewDelegate
    
    func tapCourse(courseModel: ScheduleEntryViewModel) {
        let alertView = UIAlertController(
            title: NSLocalizedString("Info", comment: ""),
            message: "\(courseModel)",
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(
            title: NSLocalizedString("OK", comment: ""),
            style: .cancel,
            handler: nil)
        alertView.addAction(okAction)
        
        let exportAction = UIAlertAction(title: "Export", style: .default, handler: {self.export($0, courseModel: courseModel)})
        alertView.addAction(exportAction)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
    func export(_ action: UIAlertAction,courseModel: ScheduleEntryViewModel) {
        let store = EKEventStore()
        store.requestAccess(to: EKEntityType.event, completion: { (granted, error) -> Void in
            if (granted) {
                if let calendarForEvent = store.defaultCalendarForNewEvents
                {
                    var interval = 1
                    var start = self.startDate
                    if courseModel.timeComment=="uKW" {
                        start = self.startDateOddWeek
                        interval = 2
                    } else if courseModel.timeComment=="gKW" {
                        start = self.startDateEvenWeek
                        interval = 2
                    }
                    
                    let event = EKEvent.init(eventStore: store)
                    event.calendar = calendarForEvent
                    event.title = courseModel.courseName
                    event.location = courseModel.room
                    event.notes = courseModel.lecturer
                    
                    var eventStartDateComponents = DateComponents()
                    eventStartDateComponents.day = courseModel.dayInWeek-1
                    eventStartDateComponents.hour = courseModel.hourSlot-1
                    let eventStartDate = self.calendar.date(byAdding: eventStartDateComponents, to: start)!
                    event.startDate = eventStartDate
                    
                    var eventEndDateComponents = DateComponents()
                    eventEndDateComponents.day = courseModel.dayInWeek-1
                    eventEndDateComponents.hour = courseModel.hourSlot-1+courseModel.courseSpan
                    let eventEndDate = self.calendar.date(byAdding: eventEndDateComponents, to: start)!
                    event.endDate = eventEndDate
                    
                    let recurrence = EKRecurrenceRule.init(recurrenceWith: .weekly, interval: interval, end: EKRecurrenceEnd.init(end: self.endDate))
                    
                    event.addRecurrenceRule(recurrence)
                    
                    DispatchQueue.main.async {
                       let controller = EKEventEditViewController()
                       controller.eventStore = store
                       controller.event = event
                       controller.editViewDelegate = self
                       self.present(controller, animated: true, completion: nil)
                    }                    
                }
            }else{
                
            }
        })
        
    }
    
    func swipeGestureRight() {
        
    }
    
    func swipeGestureLeft() {
        
    }
    
    @objc func willEnterForeground(notification: Notification) {
        let settingsSchedule = UserDefaults.standard.integer(forKey: "schedulePreference")
        if (settingsSchedule != chosenSchedule) {
            chosenSchedule = settingsSchedule
            self.title = schedules[chosenSchedule]
            interactor.execute(request: ScheduleRequest(index: chosenSchedule), displayer: self)
        }
    }
    
    // MARK: EKEventEditViewDelegate
    
    func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
