//
//  ScheduleEntryPresenter.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 19.03.19.
//  Copyright © 2019 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

class ScheduleEntryPresenter : Presenter {
    typealias Model = ScheduleEntryEntity
    typealias ViewModel = ScheduleEntryViewModel
    
    func present(model: ScheduleEntryEntity) -> ScheduleEntryViewModel {
        let hourSlot = SScheduleView.timeSlotNames.firstIndex(of: model.startTime)! + 1
        let courseSpan = SScheduleView.timeSlotNames.firstIndex(of: model.endTime)! + 1 - hourSlot
        let viewModel = ScheduleEntryViewModel(dayInWeek: model.dayOfWeek, hourSlot: hourSlot, courseSpan: courseSpan, timeComment: model.timeComment, courseName: model.name, lecturer: model.lecturer, room: model.room)
        if model.timeComment=="uKW" {
            viewModel.backColor = Colors.themeColors["Green"]
        } else if model.timeComment=="gKW" {
            viewModel.backColor = Colors.themeColors["SkyBlue"]
        }
        
        return viewModel
    }
}
