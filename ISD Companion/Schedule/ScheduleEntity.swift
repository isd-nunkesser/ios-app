//
//  ScheduleEntity.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 18.03.19.
//  Copyright © 2019 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

class ScheduleEntity : Codable {
    public var term: Int
    public var group: String
    public var fieldOfStudy: String
    public var entries: [ScheduleEntryEntity]
}
