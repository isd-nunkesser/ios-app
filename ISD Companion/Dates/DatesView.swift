//
//  DatesView.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 26.03.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import SwiftUI

struct DatesView: View {
    var body: some View {
        List {
            Section(header: Text("label.summersem")) {
                RightDetailRow(item: ItemViewModel(text: "label.reregdead", detailText: "31.01.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.modregstart", detailText: "09.03.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.lecperstart", detailText: "20.04.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.modregend", detailText: "06.04.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.freeeas", detailText: "14.04.2020 - 17.04.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.freepen", detailText: "02.06.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.modderend", detailText: "22.06.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.lecperend", detailText: "26.06.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.finexa", detailText: "29.06.2020 - 17.07.2020"))
            }
            Section(header: Text("label.wintersem")) {
                RightDetailRow(item: ItemViewModel(text: "label.reregdead", detailText: "31.07.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.modregstart", detailText: "14.09.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.lecperstart", detailText: "28.09.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.modregend", detailText: "12.10.2020"))
                RightDetailRow(item: ItemViewModel(text: "label.freechris", detailText: "23.12.2020 - 01.01.2021"))
                RightDetailRow(item: ItemViewModel(text: "label.modderend", detailText: "18.01.2021"))
                RightDetailRow(item: ItemViewModel(text: "label.lecperend", detailText: "22.01.2021"))
                RightDetailRow(item: ItemViewModel(text: "label.finexa", detailText: "25.01.2021 - 12.02.2021"))
            }
        }
        .navigationBarTitle("label.semdates")
    }
}

struct DatesView_Previews: PreviewProvider {
    static var previews: some View {
        DatesView()
    }
}

class DatesViewHostingController: UIHostingController<DatesView> {
    
    required init?(coder: NSCoder) {
        super.init(coder: coder,rootView: DatesView());
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
