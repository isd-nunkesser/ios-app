//
//  ProfsViewModel.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 31.03.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

class ProfsViewModel : ObservableObject {
    @Published var profs : [ProfViewModel] = []
}
