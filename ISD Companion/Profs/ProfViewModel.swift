//
//  ProfViewModel.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 23.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

struct ProfViewModel : Identifiable {
    let id = UUID()
    let name : String
    let field : String
    let phone : String
    let mail: String
    let link: URL    
}
