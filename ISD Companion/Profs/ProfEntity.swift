//
//  ProfEntity.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 23.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

public class ProfEntity : Codable {
public var name: String
public var field: String
public var phone: String
public var mail: String
public var link: URL

public required init(name: String, field: String, phone:String, mail:String, link:URL) {
    self.name = name
    self.field = field
    self.phone = phone
    self.mail = mail
    self.link = link
}
}
