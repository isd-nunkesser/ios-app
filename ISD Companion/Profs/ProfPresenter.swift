//
//  ProfsPresenter.swift
//  Hochschulapp
//
//  Created by Prof. Dr. Nunkesser, Robin on 06.07.17.
//  Copyright © 2017 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

struct ProfPresenter : Presenter {
    typealias Model = ProfEntity
    typealias ViewModel = ProfViewModel
    
    func present(model: ProfEntity) -> ProfViewModel {        
        return ProfViewModel(name: model.name,
                              field: model.field,
                              phone: model.phone,
                              mail: model.mail,
                              link: model.link)
    }
    
}
