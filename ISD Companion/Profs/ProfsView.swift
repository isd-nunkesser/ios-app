//
//  ProfsView.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 28.03.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import SwiftUI
import BasicCleanArch

struct ProfsView: View {
    @State var searchText = ""
    @State private var showError = false
    @State private var errorText = ""
    
    typealias ViewModelType = [ProfEntity]
    
    var profs : [ProfViewModel] = []
    
    var filteredProfs : [ProfViewModel] {
        return searchText=="" ? profs : profs.filter {
            "\($0.name)".localizedCaseInsensitiveContains(searchText) ||
                "\($0.field)".localizedCaseInsensitiveContains(searchText)
        }
    }
            
    var body: some View {
        Group {
            SearchBar(text: $searchText, placeholder: "Name")
            List(filteredProfs, rowContent: ProfRow.init)
                .alert(isPresented: $showError) { () -> Alert in
                    Alert(title: Text("Error"), message: Text(errorText),
                          dismissButton: .cancel(Text("OK")))
                }
            .navigationBarTitle("profs.title")
        }
    }
}

struct ProfsView_Previews: PreviewProvider {
    static var previews: some View {
        ProfsView()
    }
}

class ProfsViewHostingController: UIHostingController<ProfsView>, Displayer {
    typealias ViewModelType = [ProfEntity]
    var interactor = ProfsInteractor()
    var profsView = ProfsView()

    required init?(coder: NSCoder) {
        super.init(coder: coder,rootView: profsView);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.execute(request: nil, displayer: self)
    }
    
    // MARK: - Displayer
    
    func display(success: [ProfEntity], resultCode: Int) {
        let viewModel = success.map(ProfPresenter().present)
        profsView.profs = viewModel
        rootView = profsView
    }
    
    func display(failure: Error) {
        self.present(error: failure, handler: nil)
    }

}
