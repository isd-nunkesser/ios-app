//
//  ProfDummyGateway.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 23.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

class ConcreteProfGateway : FetchGateway {
    
    typealias Model = [ProfEntity]
    
    static func fetch(completion: @escaping (Result<[ProfEntity], Error>) -> Void) {
        completion(Result<[ProfEntity], Error>.success([
            ProfEntity(name: "Prof. Dr. Bernasconi", field: "Mobile Computing", phone: "", mail: "", link: URL(string: "https://www.hshl.de")!),
            ProfEntity(name: "Prof. Dr. Moriarty", field: "Mathematik", phone: "", mail: "", link: URL(string: "https://www.hshl.de")!),
            ]))
    }    
}
