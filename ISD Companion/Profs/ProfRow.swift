import SwiftUI

struct ProfRow: View {
    var prof : ProfViewModel
    
    var body: some View {
        NavigationLink(destination: WebView(url: prof.link)) {
            VStack(alignment: .leading) {
                Text(prof.name)
                    .font(.subheadline)
                Text(prof.field)
                    .font(.caption)
            }
        }
    }
}

struct ProfRow_Previews: PreviewProvider {
    static var previews: some View {
        List(0 ..< 5) { item in
            ProfRow(prof: ProfViewModel(name: "N", field: "F", phone: "P", mail: "M", link: URL(string: "https://www.hshl.de")!))
        }
    }
}
