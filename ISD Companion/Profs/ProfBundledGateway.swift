//
//  ProfBundledGateway.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 18.03.19.
//  Copyright © 2019 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

class ConcreteProfGateway : FetchFromBundleGateway {
    typealias Model = [ProfEntity]    
    static var resource: String = "profs"
}
