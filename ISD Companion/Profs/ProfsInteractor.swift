//
//  ProfInteractor.swift
//  ISD Companion
//
//  Created by Prof. Dr. Nunkesser, Robin on 23.07.18.
//  Copyright © 2018 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation
import BasicCleanArch

class ProfsInteractor : UseCase {
    typealias PresenterType = ProfPresenter
    typealias DisplayerType = ProfsViewHostingController
    typealias RequestType = Void?
    
    var presenter: ProfPresenter
    var gateway: ConcreteProfGateway
    
    init(presenter : ProfPresenter, gateway: ConcreteProfGateway) {
        self.presenter = presenter
        self.gateway = gateway
    }
    
    required convenience init(presenter : ProfPresenter) {
        self.init(presenter: presenter, gateway: ConcreteProfGateway())
    }

    convenience init() {
        self.init(presenter: ProfPresenter(), gateway: ConcreteProfGateway())
    }

    func execute(request: Void?, displayer: ProfsViewHostingController, resultCode: Int) {
        ConcreteProfGateway.fetch(completion:
            {
                switch $0 {
                case let .success(profs):
                    displayer.display(success: profs, resultCode: resultCode)
                case let .failure(error):
                    displayer.display(failure: error)
                }
        })
    }
        
}
